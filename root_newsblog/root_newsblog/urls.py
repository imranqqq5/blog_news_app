from django.contrib import admin
from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns

from sp_blog import views
from sp_blog.viewsets import router

urlpatterns = [
    path('admin/', admin.site.urls),
    path('root_newsblog/', include('sp_blog.urls')),
    path('api-auth/', include('rest_framework.urls')),
    path('api/', include(router.urls)),
    path('news/', views.news_list),
    path('news/<int:pk>', views.news_detail),

]

