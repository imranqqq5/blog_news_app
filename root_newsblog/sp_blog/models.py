from django.db import models


# Модель категории
class Category(models.Model):
    name = models.CharField(max_length=200, db_index=True)
    slug = models.SlugField(max_length=200, db_index=True, unique=True, verbose_name='Транслит')

    class Meta:
        ordering = ['name']
        verbose_name = 'Категория новостей'
        verbose_name_plural = 'Категории новостей'

    def __str__(self):
        return self.name


# Модель новостей
class News(models.Model):
    category = models.ForeignKey(Category, related_name='news', verbose_name="Категория новостей", on_delete=True)
    name = models.CharField(max_length=200, db_index=True, verbose_name="Название")
    slug = models.SlugField(max_length=200, db_index=True, verbose_name='Транслит')
    image = models.FileField(upload_to='images_for_news/%Y/%m/%d/', blank=True, verbose_name="Изображение")
    description = models.TextField(blank=True, verbose_name="Текст новостей")

    available = models.BooleanField(default=True, verbose_name="Актуальное")
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['name']
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'
        index_together = [
            ['id', 'slug']
        ]

    def __str__(self):
        return self.name


# Модель покупки статей
class BuyArticles(models.Model):
    category = models.ForeignKey(Category, related_name='buy_articles', verbose_name="Категория новостей",
                                 on_delete=True)
    name = models.CharField(max_length=200, db_index=True, verbose_name="Название")
    slug = models.SlugField(max_length=200, db_index=True, verbose_name='Транслит')
    image = models.FileField(upload_to='images_for news/%Y/%m/%d/', blank=True, verbose_name="Изображение")
    text_of_articles = models.TextField(blank=True, verbose_name="Текст новостей")

    available = models.BooleanField(default=True, verbose_name="Актуальное")
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['name']
        verbose_name = 'Купить статью'
        verbose_name_plural = 'Купить статьи'
        index_together = [
            ['id', 'slug']
        ]

    def __str__(self):
        return self.name


class Author(models.Model):
    category = models.ForeignKey(Category, related_name='author', verbose_name="Категория новостей", on_delete=True)
    news = models.ManyToManyField(News, related_name='author', verbose_name="Новости")
    firstname = models.CharField(max_length=200, db_index=True, verbose_name='Имя')
    lastname = models.CharField(max_length=200, db_index=True, verbose_name='Фамилия')
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    slug = models.SlugField(max_length=200, db_index=True, unique=True, verbose_name='Транслит')
    available = models.BooleanField(default=True, verbose_name="Действующие сотрудники")

    class Meta:
        ordering = ['lastname']
        verbose_name = 'Автор'
        verbose_name_plural = 'Авторы'

    def __str__(self):
        return self.lastname
