from django.apps import AppConfig


class SpBlogConfig(AppConfig):
    name = 'sp_blog'
