from rest_framework import serializers

from sp_blog.models import News


class NewsSerializer(serializers.ModelSerializer):
    """
    News
    """

    class Meta:
        model = News
        fields = ('category',
                  'name',
                  'image',
                  'description',
                  'available',
                  'created',
                  'updated')


class BuyArticlesSerializer(serializers.ModelSerializer):
    """
    BuyArticles
    """

    class Meta:
        model = News
        fields = ('category',
                  'name',
                  'image',
                  'text_of_articles',
                  'available',
                  'created',
                  'updated')


class AuthorSerializer(serializers.ModelSerializer):
    """
    Author
    """

    class Meta:
        model = News
        fields = ('category',
                  'news',
                  'firstname',
                  'lastname',
                  'created',
                  'updated',
                  'available')
