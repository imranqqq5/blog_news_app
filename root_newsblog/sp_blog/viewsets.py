# TODO: CREATE, GET, UPDATE
from rest_framework import routers, serializers, viewsets, status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.views import APIView

from sp_blog.models import News
from sp_blog.serializers import NewsSerializer


class NewsViewSet(viewsets.ModelViewSet):
    queryset = News.objects.all()
    serializer_class = NewsSerializer
    # permission_classes =
#
# class DetailView(APIView):
#     def get_object(self, pk):
#         return News.objects.get(pk=pk)
#
#     serializer_class = NewsSerializer


router = routers.DefaultRouter()
router.register(r'news', NewsViewSet)


# @api_view(['GET', 'POST'])
# def news_list(request):
#     """
#     List all code snippets, or create a new snippet.
#     """
#     if request.method == 'GET':
#         queryset = News.objects.all()
#         serializer = NewsSerializer(queryset, many=True)
#         return Response(serializer.data)
#
#     elif request.method == 'POST':
#         serializer = NewsSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
#
#
# class DetailView(APIView):
#     def get_object(self, pk):
#         return News.objects.get(pk=pk)
#
#     def patch(self, request, pk):
#         testmodel = self.get_object(pk)
#         serializer = NewsSerializer(testmodel, data=request.data,
#                                          partial=True)  # set partial=True to update a data partially
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
